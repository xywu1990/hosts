function FindProxyForURL(url, host)
{ 
     if (shExpMatch(host, "*youtube*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*ytimg.com*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*ggpht*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*youtu.be*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*google*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*wikipedia*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*wikimedia*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*facebook*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*fbcdn*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*fb.com*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*fb.me*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*fbsdx*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*messenger*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*twimg.com*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*twitter*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*t.co*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*linkedin*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*redd*"))
          return "PROXY 192.168.1.18:8080"
          
     if (shExpMatch(host, "*medium*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*aps.org*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*nature*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*science*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*iop.org*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*quora*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*githubusercontent*"))
          return "PROXY 192.168.1.18:8080"

     if (shExpMatch(host, "*zoom*"))
          return "PROXY 192.168.1.18:8080"
     
     if (shExpMatch(host, "*ibm.com*"))
          return "PROXY 192.168.1.18:8080"


	return "DIRECT";
}
